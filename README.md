# CS371p: Allocator

* Name: Jordan Taylor | Mark Ramirez

* EID: jft579 | mar7468

* GitLab ID: jordanfjtaylor | markaramirez8

* HackerRank ID: jordanfjtaylor | markaramirez8

* Git SHA: 47fca9042d8ae9947d192eec4a7c30e3524c6c81

* GitLab Pipelines: https://gitlab.com/jordanfjtaylor/cs371p-allocator/pipelines

* Estimated completion time: 6 hours

* Actual completion time: 12 hours
