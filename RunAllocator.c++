// --------------
// RunAllocator.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.h"

using namespace std;

// ----
// main
// ----

int main () {
    using namespace std;
    allocator_solve(cin, cout);
    return 0;
}