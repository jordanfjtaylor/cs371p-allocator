#include "Allocator.h"
#include <iostream>
#include <cmath>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <sstream>
using namespace std;

template<typename T>
T FromString(const std::string& str)
{
    std::istringstream ss(str);
    T ret;
    ss >> ret;
    return ret;
}

void allocator_solve(istream& r, ostream& w) {
    const int HEAP_SIZE = 1000;
    const int ITEM_SIZE = 8;
    string line;
    getline(r, line);

    int num_tests = FromString<int>(line);
    getline(r, line); // skip newline


    // Loop through each test case,
    // creating a new allocator and performing
    // the requested operations for each one.
    // These tests assume HEAP_SIZE = 1000 and ITEM_SIZE = 8
    for(int i = 0; i < num_tests; i++) {
        my_allocator<double, HEAP_SIZE> x;
        while(getline(r, line) && line.length() != 0) {
            int request = FromString<int>(line);

            if(request < 0) {
                // We need to find the correct block and deallocate it
                int count = 0;
                int byte_index = 0;
                request *= -1;

                while(byte_index < HEAP_SIZE) {

                    int curr_sentinel = x[byte_index];
                    if(curr_sentinel < 0) {
                        count++;
                    }

                    if(count == request)
                        break;

                    byte_index += (curr_sentinel > 0 ? curr_sentinel + 8 : (curr_sentinel * -1) + 8);
                }
                double* p = reinterpret_cast<double*>(&x[byte_index + 4]);
                x.deallocate(p, ITEM_SIZE);
            }
            else {
                // Just allocate
                x.allocate(request);
            }
        }

        // Print the results
        auto b = begin(x);
        auto e = end(x);

        while(b != e) {
            w << *b << " \n"[++b == e];
        }
    }
}