// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // isteam, ostream

using namespace std;
// ---------
// Allocator
// ---------

// -------------
// allocator_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 * Reads the input and prints the results
 */
void allocator_solve (istream& r, ostream& w);

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Ensure that the sentinel values + sentinel size = heap size
     */
    bool valid () const {
        auto b = this->begin();
        auto e = this->end();

        int sum = 0;
        while(b != e) {
            sum += (*b > 0 ? *b : *b * -1) + 8;
            ++b;
        }

        //cout << "Status: " << (sum == N) << endl;
        return sum == N;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs._p == rhs._p);  // our code
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p; // our code
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            char* ptr = reinterpret_cast<char*>(_p);
            _p = reinterpret_cast<int*>(*_p > 0 ? ptr + *_p + 8 : ptr + (*_p * -1) + 8); // our code
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* ptr = reinterpret_cast<char*>(_p);
            _p = reinterpret_cast<int*>(*reinterpret_cast<int*>(ptr - 4) > 0 ? ptr - 8 - *reinterpret_cast<int*>(ptr - 4) : ptr - 8 - (-1 * (*reinterpret_cast<int*>(ptr - 4))));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
            return false;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            char* ptr = reinterpret_cast<char*>(_p);
            _p = reinterpret_cast<int*>(*_p > 0 ? ptr + *_p + 8 : ptr + (*_p * -1) + 8);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            char* ptr = reinterpret_cast<char*>(_p);
            _p = reinterpret_cast<int*>(*reinterpret_cast<int*>(ptr - 4) > 0 ? ptr - 8 - *reinterpret_cast<int*>(ptr - 4) : ptr - 8 - (-1 * (*reinterpret_cast<int*>(ptr - 4))));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if(N < (sizeof(T) + (2*sizeof(int))))
            throw std::bad_alloc();

        (*this)[0] = N-8;
        (*this)[N-4] = N-8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        int requested_bytes = n * sizeof(T);
        const int MIN_SIZE = sizeof(T) + 8;
        int byte_index = 0;

        if(n <= 0)
            throw std::bad_alloc();

        while(byte_index < N) {
            int sentinel_val = (*this)[byte_index];

            // check if free
            if(sentinel_val < 0) {
                byte_index += (sentinel_val * -1) + 8;
                continue;
            }

            // check if we can allocate using this one
            if(sentinel_val >= requested_bytes) {

                // check if we should split
                if(sentinel_val - requested_bytes >= MIN_SIZE) {

                    // old start sentinel
                    (*this)[byte_index] = requested_bytes * -1;

                    // old end sentinel
                    (*this)[byte_index + requested_bytes + 4] = requested_bytes * -1;

                    // new start sentinel
                    (*this)[byte_index + 8 + requested_bytes] = sentinel_val - requested_bytes - 8;

                    // new end sentinel
                    (*this)[byte_index + 8 + requested_bytes + 4 + (sentinel_val - requested_bytes - 8)] = sentinel_val - requested_bytes - 8;
                }
                else { // just allocate using this one
                    // start sentinel
                    (*this)[byte_index] = sentinel_val * -1;

                    // end sentinel
                    (*this)[byte_index + sentinel_val + 4] = sentinel_val * -1;
                }
                assert(valid());
                return reinterpret_cast<pointer>(&a[byte_index+4]);
            }

            byte_index += sentinel_val + 8;
        }
        assert(valid());
        throw std::bad_alloc();
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Free the block that p points to and merge with left & right if possible
     */
    void deallocate (pointer p, size_type n) {

        int byte_index = (reinterpret_cast<char*>(p) - 4) - reinterpret_cast<char*>(&((*this)[0]));

        int new_sentinel_val = (*this)[byte_index] * -1;

        int left_position = byte_index;
        int right_position = byte_index + ((*this)[byte_index] *-1) + 4;

        // Throw invalid arg exception if
        // this does not resemble a correct block
        if((*this)[left_position] != (*this)[right_position])
            throw std::invalid_argument("Invalid argument to deallocate.");

        // check left
        if(byte_index != 0 && (*this)[byte_index - 4] > 0) {
            // merge left
            left_position = byte_index - 8 - (*this)[byte_index - 4];
            new_sentinel_val = new_sentinel_val + 8 + (*this)[byte_index - 4];
        }

        // check right
        int right_start = byte_index + ((*this)[byte_index] *-1) + 8;
        if(right_start < N && (*this)[right_start] > 0) {
            // merge right
            right_position = right_start + (*this)[right_start] + 4;
            new_sentinel_val = new_sentinel_val + 8 + (*this)[right_start];
        }

        // left sentinel
        (*this)[left_position] = new_sentinel_val;

        // right sentinel
        (*this)[right_position] = new_sentinel_val;
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(const_cast<int*>(reinterpret_cast<const int*>(&a[0])));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(const_cast<int*>(reinterpret_cast<const int*>(&a[N])));
    }
};

#endif // Allocator_h
